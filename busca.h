#ifndef BUSCA
#define BUSCA

#include <posicao.h>
#include <queue>
#include <vector>
#include <iostream>
#include <functional>
#include <stack>
#include <QDebug>

    using namespace std;

/*struct greater : binary_function <T, T, bool> {
  bool operator() (const T& x, const T& y) const {
      return x>y;
  }
}; */

struct custo : std::binary_function <Posicao*, Posicao*, bool>{
    bool operator() (const Posicao* one, Posicao* two) const{
        return one->getF() > two->getF();
    }
};

void heuristica (int largura, int altura, float pesoVertical, float pesoHorizontal, Posicao *maze, Posicao *chegada){

    for(int i=0; i<altura; i++){
        for(int j=0; j<largura; j++){
            maze[i*largura +j].setH(pesoHorizontal * abs(chegada->getX() - j) +
                                    pesoVertical * abs(chegada->getY() - i));            
            }
        }
    for(int i=0; i<altura; i++){
        for(int j=0; j<largura;j++){
            qDebug() << maze[i*largura + j].getY() << "x" << maze[i*largura + j].getX() << "tem  H igual a: "
                    << maze[i*largura + j].getH();
            }   qDebug() << endl;

    }
}

void definirRota(Posicao *celula){

    //Posicao *aux = celula->getPai();

    if(celula->getTipo() != INICIO ){

        celula->getCelula()->setBackgroundColor(Qt::darkGreen);

        definirRota(celula->getPai());

    }
    else
    {
        celula->getCelula()->setBackgroundColor(Qt::green);
    }

}

void aEstrela (double pesoVertical, double pesoHorizontal, double pesoDiagonal, Posicao *partida, Posicao *chegada){
    priority_queue < Posicao*, std::vector<Posicao*>, custo > fila;

    double g, f;

    Posicao *teste;
    fila.push(partida);

    do{
        teste = fila.top();
        fila.pop();
        teste->getCelula()->setBackgroundColor(Qt::blue);
        for(int i=0; i<8; i++){
            //qDebug() << i << " de " << teste->getY() << "x" << teste->getX() << " é " << teste->getTipo();
            if(teste->getVizinho(i) != nullptr && teste->getVizinho(i)->getTipo() != OBSTACULO){
                /*  #0  #1  #2
                 *  #7  Po  #3      Vizinhança
                    #6  #5  #4  */
                if(i == 1 || i == 5){
                    g = pesoVertical + teste->getG();
                }
                else if(i == 3 || i == 7){
                    g = pesoHorizontal + teste->getG();
                }
                else{
                    g = pesoDiagonal + teste->getG();
                }

                //qDebug() << teste->getG() << "g do nó";
                //qDebug()  << teste->getY() << "x" << teste->getX() <<  "->" << teste->getVizinho(i)->getG() << "eh o g do vizinho " << i;

                if(teste->getVizinho(i)->getPai() == nullptr ||
                   (teste->getVizinho(i)->getPai() != nullptr && teste->getVizinho(i)->getG() > g )){
        /*  Só vai entrar nessa parte se:
         *  Essa posição não tiver sido acessada
         *  A posição foi acessada, mas seu G  é maior que o G da posição de teste + custo  */                                        
                        f = teste->getVizinho(i)->getH() + g;

                        teste->getVizinho(i)->getCelula()->setBackgroundColor(Qt::gray);
                        teste->getVizinho(i)->setPai(teste);
                        teste->getVizinho(i)->setF(f);
                        teste->getVizinho(i)->setG(g);

                        fila.push(teste->getVizinho(i));

                }
            }
        }

    }while(teste->getTipo() != FIM && !fila.empty());

    if(chegada->getPai() != nullptr){

         definirRota(chegada);
    }

}

#endif // BUSCA

