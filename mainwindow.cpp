#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <math.h>
#include <busca.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_dial_x_valueChanged(int value)
{
    ui->size_x->display(value);
}

void MainWindow::on_dial_y_valueChanged(int value)
{
    ui->size_y->display(value);
}


void MainWindow::on_value_vert_valueChanged(double arg1)
{
    int value = arg1;
    float teste = value;

    if((arg1-teste)>0.5)
        ui->progress_vert->setValue(value+1);
    else
        ui->progress_vert->setValue(value);

}

void MainWindow::on_value_horiz_valueChanged(double arg1)
{
    int value = arg1;
    float teste = value;

    if((arg1-teste)>0.5)
        ui->progress_horiz->setValue(value+1);
    else
        ui->progress_horiz->setValue(value);
}

void MainWindow::on_value_diag_valueChanged(double arg1)
{
    int value = arg1;
    float teste = value;

    if((arg1-teste)>0.5)
        ui->progress_diag->setValue(value+1);
    else
        ui->progress_diag->setValue(value);
}

void MainWindow::on_diag_autom_clicked(bool checked)
{
    float result, value_horiz, value_vert;
    value_horiz = ui->value_horiz->value();
    value_vert = ui->value_vert->value();
    result = sqrt(pow(value_horiz,2)+pow(value_vert,2));

    if(checked){
        ui->value_diag->setValue(result);
        ui->value_diag->setEnabled(false);
    }
    else{
        ui->value_diag->setEnabled(true);
    }
}

void MainWindow::on_value_vert_editingFinished()
{
    if(ui->diag_autom->isChecked()){
        float result, value_horiz, value_vert;
        value_horiz = ui->value_horiz->value();
        value_vert = ui->value_vert->value();
        result = sqrt(pow(value_horiz,2)+pow(value_vert,2));

        ui->value_diag->setValue(result);
    }
}

void MainWindow::on_value_horiz_editingFinished()
{
    if(ui->diag_autom->isChecked()){
        float result, value_horiz, value_vert;
        value_horiz = ui->value_horiz->value();
        value_vert = ui->value_vert->value();
        result = sqrt(pow(value_horiz,2)+pow(value_vert,2));

        ui->value_diag->setValue(result);
    }
}

void MainWindow::criarMapa(int largura, int altura){

    ui->mapa->setColumnCount(largura);
    ui->mapa->setRowCount(altura);

    /*int vertical, horizontal;

    horizontal = 1100/largura;
    vertical = 450/altura; */

    ui->mapa->horizontalHeader()->setDefaultSectionSize(60);
    ui->mapa->verticalHeader()->setDefaultSectionSize(60);

    maze = new Posicao[altura*largura];

    QTableWidgetItem *elemento;

    for(int i=0; i<altura; i++){
        for(int j=0; j<largura; j++){

            elemento = new QTableWidgetItem();
            ui->mapa->setItem(i,j, elemento);

            maze[i*largura + j].setTipo(LIVRE);
            maze[i*largura + j].setCelula(elemento);
            maze[i*largura + j].setPai(nullptr);
            maze[i*largura + j].setPrimeiro(nullptr);
            maze[i*largura + j].setX(j);
            maze[i*largura + j].setY(i);

            /*  #0  #1  #2
             *  #7  Po  #3      Vizinhança
                #6  #5  #4  */

            if(i==0 || j==0)
                maze[i*largura + j].setVizinho(0, nullptr);
            else
                maze[i*largura + j].setVizinho(0, &maze[(i-1)*largura + (j-1)]);

            if(i==0)
                maze[i*largura + j].setVizinho(1, nullptr);
            else
                maze[i*largura + j].setVizinho(1, &maze[(i-1)*largura + j]);

            if(i==0 || j==(largura-1))
                maze[i*largura + j].setVizinho(2, nullptr);
            else
                maze[i*largura + j].setVizinho(2, &maze[(i-1)*largura + (j+1)]);

            if(j==(largura-1))
                maze[i*largura + j].setVizinho(3, nullptr);
            else
                maze[i*largura + j].setVizinho(3, &maze[i*largura + (j+1)]);

            if(i==(altura-1) || j==(largura-1))
                maze[i*largura + j].setVizinho(4, nullptr);
            else
                maze[i*largura + j].setVizinho(4, &maze[(i+1)*largura + (j+1)]);

            if(i==(altura-1))
                maze[i*largura + j].setVizinho(5, nullptr);
            else
                maze[i*largura + j].setVizinho(5, &maze[((i+1)*largura + j)]);

            if(i==(altura-1) || j==0)
                maze[i*largura + j].setVizinho(6, nullptr);
            else
                maze[i*largura + j].setVizinho(6, &maze[(i+1)*largura + (j-1)]);

            if(j==0)
                maze[i*largura + j].setVizinho(7, nullptr);
            else
                maze[i*largura +j].setVizinho(7, &maze[i*largura + (j-1)]);
        }
    }

}


void MainWindow::on_criarMapa_clicked(){

    if(!MapaCriado)
    {
        MapaCriado = true;
        criarMapa(ui->dial_x->value(), ui->dial_y->value());
        ui->dial_x->setEnabled(false);
        ui->dial_y->setEnabled(false);

    }
    else
        QMessageBox::information(this,tr("Aviso!"),tr("Apague o mapa atual antes da criação de outro."));
}

void MainWindow::apagarMapa(){

    ui->mapa->setColumnCount(0);
    ui->mapa->setRowCount(0);
    delete[] maze;

    ui->dial_x->setEnabled(true);
    ui->dial_y->setEnabled(true);

    ui->dial_x->setValue(5);
    ui->dial_y->setValue(5);

    ui->value_horiz->setEnabled(true);
    ui->value_vert->setEnabled(true);
    ui->value_diag->setEnabled(true);

    ui->value_diag->setValue(1);
    ui->value_horiz->setValue(1);
    ui->value_vert->setValue(1);

    ui->diag_autom->setChecked(false);

    ui->calcularRota->setEnabled(true);
    RotaCalculada = false;

}

void MainWindow::on_pushButton_clicked()
{
    if(MapaCriado)
    {
        apagarMapa();
        MapaCriado = false;
        PartidaDefinida = false;
        ChegadaDefinida = false;
    }
    else
        QMessageBox::information(this,tr("Aviso!"),tr("Não existe mapa para ser criado."));
}

bool MainWindow::isLivre(QTableWidgetItem *item){
    return maze[(item->row())*(ui->dial_x->value())+item->column()].getTipo() == LIVRE;
}


bool MainWindow::isObstaculo(QTableWidgetItem *item){
    return maze[(item->row())*(ui->dial_x->value())+item->column()].getTipo() == OBSTACULO ? true : false;
}

bool MainWindow::isInicio(QTableWidgetItem *item){
    return maze[(item->row())*(ui->dial_x->value())+item->column()].getTipo() == INICIO ? true : false;
}

bool MainWindow::isFim(QTableWidgetItem *item){
    return maze[(item->row())*(ui->dial_x->value())+item->column()].getTipo() == FIM ? true : false;
}

void MainWindow::on_mapa_itemClicked(QTableWidgetItem *item)
{
    if(!RotaCalculada){
        QIcon icon;

        if(isLivre(item)){
            if(!PartidaDefinida){

                PartidaDefinida = true;
                partida = &maze[(item->row())*(ui->dial_x->value())+item->column()];

                partida->setPrimeiro(partida);
                partida->setPai(partida);
                partida->setY(item->row());
                partida->setX(item->column());


                maze[(item->row())*(ui->dial_x->value())+item->column()].setTipo(INICIO);

                icon.addFile(QString(":/fotos/mario.png"), QSize(), QIcon::Normal, QIcon::Off);
                item->setIcon(icon);
            }

            else if(!ChegadaDefinida){
                ChegadaDefinida = true;
                chegada = &maze[(item->row())*(ui->dial_x->value())+item->column()];

                chegada->setY(item->row());
                chegada->setX(item->column());


                maze[(item->row())*(ui->dial_x->value())+item->column()].setTipo(FIM);

                icon.addFile(QString(":/fotos/yoshi.png"), QSize(), QIcon::Normal, QIcon::Off);
                item->setIcon(icon);
            }

            else{
                maze[(item->row())*(ui->dial_x->value())+item->column()].setTipo(OBSTACULO);

                item->setBackgroundColor(Qt::black);
            }
        }

        else if(isObstaculo(item)){
            maze[(item->row())*(ui->dial_x->value())+item->column()].setTipo(LIVRE);

            item->setBackgroundColor(Qt::white);
            icon.addFile(QString(":"), QSize(), QIcon::Normal, QIcon::Off);
            item->setIcon(icon);            
        }

        else if(isInicio(item)){
            PartidaDefinida = false;

            partida->setPrimeiro(nullptr);
            partida->setPai(nullptr);

            partida = nullptr;

            maze[(item->row())*(ui->dial_x->value())+item->column()].setTipo(LIVRE);

            icon.addFile(QString(":"), QSize(), QIcon::Normal, QIcon::Off);
            item->setIcon(icon);
        }

        else{
            ChegadaDefinida = false;

            chegada->setY(item->row());
            chegada->setX(item->column());

            chegada = nullptr;

            maze[(item->row())*(ui->dial_x->value())+item->column()].setTipo(LIVRE);

            icon.addFile(QString(":"), QSize(), QIcon::Normal, QIcon::Off);
            item->setIcon(icon);
        }


    }
}

void MainWindow::on_actionCarregar_Mapa_triggered()
{  if(MapaCriado == false){
           QString filename = QFileDialog::getOpenFileName(this,
                                                   tr("Open File"),
                                                   "/home/flavio/Documentos/Controle Inteligente/ControleInteligente/Mapas",
                                                   "Text File (*.txt)");

           MapaCriado = true;
           PartidaDefinida = false;
           ChegadaDefinida = false;

           fstream File;

           File.open(filename.toStdString());

           if(!File){
               QMessageBox::information(this,tr("Aviso!"),tr("Não foi possível carregar o arquivo!"));
           }

           else{
               ui->diag_autom->setChecked(true);
               ui->value_diag->setEnabled(false);
               ui->dial_x->setEnabled(false);
               ui->dial_y->setEnabled(false);

               string line;
               int next, cont=0, contline = 0, conti, contj = 0;
               int nl, nc, aux;
               float ch, cv;

               while(getline(File, line)){
                   if(line.empty()) continue;

                   istringstream iss(line);

                   while(iss >> next){
                       if(cont == 0){
                           nl = next;
                           ui->dial_y->setValue(nl);
                       }
                       else if(cont == 1){
                           nc = next;
                           ui->dial_x->setValue(nc);
                       }
                       else if(cont == 2){
                           ch = (float) next;
                           ui->value_horiz->setValue(ch);
                       }
                       else if(cont == 3){
                           cv = (float) next;
                           ui->value_vert->setValue(cv);
                           float cd = sqrt(pow(cv,2)+pow(ch,2));
                           ui->value_diag->setValue(cd);
                       }
                       else{
                           if(cont == 4){
                               ui->mapa->setColumnCount(nc);
                               ui->mapa->setRowCount(nl);

                               ui->mapa->horizontalHeader()->setDefaultSectionSize(60);
                               ui->mapa->verticalHeader()->setDefaultSectionSize(60);

                               maze = new Posicao[nl*nc];


                           }

                           aux = (contj-4)%nc;

                           QTableWidgetItem *elemento = new QTableWidgetItem();
                           ui->mapa->setItem(conti,aux, elemento);

                           maze[conti*nc + aux].setTipo(next);
                           maze[conti*nc + aux].setCelula(elemento);
                           maze[conti*nc + aux].setPai(nullptr);
                           maze[conti*nc + aux].setPrimeiro(nullptr);
                           maze[conti*nc + aux].setX(aux);
                           maze[conti*nc + aux].setY(conti);

                           /*  #0  #1  #2
                            *  #7  Po  #3      Vizinhança
                               #6  #5  #4  */

                           if(conti==0 || aux==0)
                               maze[conti*nc + aux].setVizinho(0, nullptr);
                           else
                               maze[conti*nc + aux].setVizinho(0, &maze[(conti-1)*nc + (aux-1)]);

                           if(conti==0)
                               maze[conti*nc + aux].setVizinho(1, nullptr);
                           else
                               maze[conti*nc + aux].setVizinho(1, &maze[(conti-1)*nc + aux]);

                           if(conti==0 || aux==(nc-1))
                               maze[conti*nc + aux].setVizinho(2, nullptr);
                           else
                               maze[conti*nc + aux].setVizinho(2, &maze[(conti-1)*nc + (aux+1)]);

                           if(aux==(nc-1))
                               maze[conti*nc + aux].setVizinho(3, nullptr);
                           else
                               maze[conti*nc + aux].setVizinho(3, &maze[conti*nc + (aux+1)]);

                           if(conti==(nl-1) || aux==(nc-1))
                               maze[conti*nc + aux].setVizinho(4, nullptr);
                           else
                               maze[conti*nc + aux].setVizinho(4, &maze[(conti+1)*nc + (aux+1)]);

                           if(conti==(nl-1))
                               maze[conti*nc + aux].setVizinho(5, nullptr);
                           else
                               maze[conti*nc + aux].setVizinho(5, &maze[(conti+1*nc + aux)]);

                           if(conti==(nl-1) || aux==0)
                               maze[conti*nc + aux].setVizinho(6, nullptr);
                           else
                               maze[conti*nc + aux].setVizinho(6, &maze[(conti+1)*nc + (aux-1)]);

                           if(aux==0)
                               maze[conti*nc + aux].setVizinho(7, nullptr);
                           else
                               maze[conti*nc +aux].setVizinho(7, &maze[conti*nc + (aux-1)]);

                           QIcon icon;

                           if(next == 0){
                               ui->mapa->item(conti, aux)->setBackgroundColor(Qt::white);
                           }
                           else if(next == 1){
                               ui->mapa->item(conti, aux)->setBackgroundColor(Qt::black);
                           }

                           else if(next == 2){
                               partida = &maze[conti*nc + aux];
                               partida->setPrimeiro(partida);
                               partida->setPai(partida);
                               partida->setY(conti);
                               partida->setX(aux);



                               icon.addFile(QString(":/fotos/mario.png"), QSize(), QIcon::Normal, QIcon::Off);
                               ui->mapa->item(conti, aux)->setIcon(icon);

                               PartidaDefinida = true;
                           }

                           else if(next == 3){
                               chegada = &maze[conti*nc + aux];

                               chegada->setY(conti);
                               chegada->setX(aux);


                               icon.addFile(QString(":/fotos/yoshi.png"), QSize(), QIcon::Normal, QIcon::Off);
                               ui->mapa->item(conti, aux)->setIcon(icon);

                               ChegadaDefinida = true;
                           }
                       }
                       cont++;
                       contj++;

                   }
                contline++;
                conti = contline -1;
               }
           }
        }
    else
        QMessageBox::information(this,tr("Aviso!"),tr("Apague o mapa atual antes da criação de outro."));
}

void MainWindow::on_calcularRota_clicked()
{
    if(MapaCriado == true && PartidaDefinida == true && ChegadaDefinida ==true && RotaCalculada == false){

        heuristica(ui->dial_y->value(), ui->dial_x->value(), ui->value_vert->value(), ui->value_horiz->value(), maze, chegada);

        aEstrela(ui->value_vert->value(), ui->value_horiz->value(), ui->value_diag->value(), partida, chegada);

        RotaCalculada = true;

        ui->calcularRota->setEnabled(false);
    }
}
