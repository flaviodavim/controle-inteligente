#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <posicao.h>
#include <QMessageBox>
#include <QString>
#include <QFileDialog>
#include <fstream>
#include <iostream>
#include <string>
#include <sstream>

    using namespace std;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:

    void on_dial_x_valueChanged(int value);

    void on_dial_y_valueChanged(int value);

    void on_value_vert_valueChanged(double arg1);

    void on_value_horiz_valueChanged(double arg1);

    void on_value_diag_valueChanged(double arg1);

    void on_diag_autom_clicked(bool checked);

    void on_value_vert_editingFinished();

    void on_value_horiz_editingFinished();

    void criarMapa(int largura, int altura);

    void on_criarMapa_clicked();

    void apagarMapa();

    void on_pushButton_clicked();

    bool isLivre(QTableWidgetItem *item);

    bool isObstaculo(QTableWidgetItem *item);

    bool isInicio(QTableWidgetItem *item);

    bool isFim(QTableWidgetItem *item);

    void on_mapa_itemClicked(QTableWidgetItem *item);

    void on_actionCarregar_Mapa_triggered();

    void on_calcularRota_clicked();

private:
    Ui::MainWindow *ui;

    bool MapaCriado = false;
    bool RotaCalculada = false;
    bool PartidaDefinida = false;
    bool ChegadaDefinida = false;

    Posicao * maze;
    Posicao *partida, *chegada;

    ifstream File;
};

#endif // MAINWINDOW_H
