#ifndef POSICAO_H
#define POSICAO_H

#define LIVRE 0
#define OBSTACULO 1
#define INICIO 2
#define FIM 3

#include <QTableWidgetItem>

class Posicao
{
public:
    Posicao();

    int getX() const;
    void setX(int value);

    int getY() const;
    void setY(int value);

    double getF() const;
    void setF(double value);

    double getG() const;
    void setG(double value);

    double getH() const;
    void setH(double value);

    int getTipo();
    void setTipo(int value);

    Posicao* getVizinho(int i);
    void setVizinho(int i, Posicao *value);

    Posicao* getPai();
    void setPai(Posicao *pai);

    Posicao* getPrimeiro() const;
    void setPrimeiro(Posicao *primeiro);

    QTableWidgetItem* getCelula() const;
    void setCelula(QTableWidgetItem *value);

private:
    int x, y;
    double f, g, h;
    Posicao *vizinhos[8];
    Posicao *pai, *primeiro;
    QTableWidgetItem *celula;
    int tipo;
};


#endif // POSICAO_H
