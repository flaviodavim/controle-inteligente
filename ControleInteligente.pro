#-------------------------------------------------
#
# Project created by QtCreator 2016-02-25T17:01:56
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ControleInteligente
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    posicao.cpp

HEADERS  += mainwindow.h \
    posicao.h \
    busca.h

FORMS    += mainwindow.ui

QMAKE_CXXFLAGS += -std=c++0x

QT += core

RESOURCES += \
    recursos.qrc
