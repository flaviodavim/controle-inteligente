#include "posicao.h"

Posicao::Posicao()
{
    this->f=0;
    this->g=0;
    this->h=0;

    this->tipo=LIVRE;
}

int Posicao::getX() const
{
    return this->x;
}

void Posicao::setX(int value)
{
    this->x = value;
}

int Posicao::getY() const
{
    return this->y;
}

void Posicao::setY(int value)
{
    this->y = value;
}

double Posicao::getF() const
{
    return this->f;
}

void Posicao::setF(double value)
{
    this->f = value;
}

double Posicao::getG() const
{
    return this->g;
}

void Posicao::setG(double value)
{
    this->g = value;
}

double Posicao::getH() const
{
    return this->h;
}

void Posicao::setH(double value)
{
    this->h = value;
}

int Posicao::getTipo()
{
    return this->tipo;
}

void Posicao::setTipo(int value)
{
    this->tipo = value;
}

Posicao* Posicao::getVizinho(int i)
{
    return this->vizinhos[i];
}

void Posicao::setVizinho(int i, Posicao *value)
{
    this->vizinhos[i] = value;
}

Posicao* Posicao::getPai()
{
    return this->pai;
}

void Posicao::setPai(Posicao *pai)
{
    this->pai = pai;
}

Posicao* Posicao::getPrimeiro() const
{
    return this->primeiro;
}

void Posicao::setPrimeiro(Posicao *primeiro)
{
    this->primeiro = primeiro;
}

QTableWidgetItem* Posicao::getCelula() const
{
    return this->celula;
}

void Posicao::setCelula(QTableWidgetItem *value)
{
    this->celula = value;
}
